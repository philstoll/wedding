# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'

@script = <<SCRIPT
# Fix for https://bugs.launchpad.net/ubuntu/+source/livecd-rootfs/+bug/1561250
if ! grep -q "ubuntu-xenial" /etc/hosts; then
    echo "127.0.0.1 ubuntu-xenial" >> /etc/hosts
fi

# Install dev dependencies
#curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
#echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

# Install Prod Dependencies
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y apache2 php7.1 php7.1-bz2 php7.1-cli php7.1-curl php7.1-intl php7.1-json php7.1-mbstring php7.1-opcache php7.1-soap php7.1-sqlite3 php7.1-xml php7.1-xsl php7.1-zip libapache2-mod-php7.1
#Back to Dev Dependencies
apt-get install -y nodejs php7.1-xml
npm install --global gulp-cli

# Xdebug
apt-get install -y php-xdebug

sed -i '$a xdebug.default_enable = 1' /etc/php/7.1/mods-available/xdebug.ini
sed -i '$a xdebug.remote_enable = 1' /etc/php/7.1/mods-available/xdebug.ini
sed -i '$a xdebug.remote_connect_back = 1' /etc/php/7.1/mods-available/xdebug.ini
sed -i '$a xdebug.idekey = "PHPSTORM"' /etc/php/7.1/mods-available/xdebug.ini
sed -i '$a xdebug.remote_autostart = 1' /etc/php/7.1/mods-available/xdebug.ini
sed -i '$a xdebug.remote_host = 10.0.2.2' /etc/php/7.1/mods-available/xdebug.ini

debconf-set-selections <<< 'mysql-server mysql-server/root_password password Blues'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password Blues'
apt-get install -y mysql-server
mysql --user=root --password=Blues -e "CREATE DATABASE wedding DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci"


# Configure Apache
echo "<VirtualHost *:80>
    ServerName wedding.test
	DocumentRoot /var/www/public
	AllowEncodedSlashes On

	<Directory /var/www/public>
		Options +Indexes +FollowSymLinks
		DirectoryIndex index.php index.html
		Order allow,deny
		Allow from all
		AllowOverride All
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>" > /etc/apache2/sites-available/000-default.conf
a2enmod rewrite
service apache2 restart

rm -r /var/www/html

if [ -e /usr/local/bin/composer ]; then
    /usr/local/bin/composer self-update
else
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
fi

# Reset home directory of vagrant user
if ! grep -q "cd /var/www" /home/ubuntu/.profile; then
    echo "cd /var/www" >> /home/ubuntu/.profile
fi

SCRIPT

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = 'ubuntu/xenial64'
  config.vm.network "forwarded_port", guest: 80, host: 8080, auto_correct: true
  config.vm.network "private_network", ip: '192.168.56.109'
  config.vm.synced_folder '.', '/var/www'
  config.vm.provision 'shell', inline: @script

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1024"]
    vb.customize ["modifyvm", :id, "--name", "Wedding Expressive Web Application"]
    vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]
  end
end
