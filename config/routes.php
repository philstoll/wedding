<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;

return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container) : void {
    $app->get('/', App\Handler\HomePageHandler::class, 'home');
    $app->get('/registry', App\Handler\RegistryHandler::class, 'registry');
    $app->get('/details', \App\Handler\DetailsHandler::class, 'details');
    $app->get('/rsvp', \App\Handler\RsvpPage::class, 'rsvp');
    $app->post('/rsvp', [\App\Handler\RsvpHandler::class, \App\Handler\RsvpPage::class], 'rsvp-handler');
    $app->get('/photos', \App\Handler\PhotosHandler::class, 'photos');
    $app->get('/amazon', App\Handler\RedirectHandler::class, 'amazon');
    $app->get('/honeymoon', App\Handler\RedirectHandler::class, 'honeymoon');
    $app->get('/bbb', App\Handler\RedirectHandler::class, 'bbb');
};
