<?php

declare(strict_types=1);

chdir(dirname(__DIR__));

$db = new SQLite3('db/wedding.db');
$statementString = file_get_contents('db/table-structure.sql');

foreach (explode(';', $statementString) as $index => $tableSchema) {
    if (false === empty($tableSchema)) {
        $statement = $db->prepare($tableSchema);
        $result = $statement->execute();
        echo 'Created table: ' . ($index + 1) . PHP_EOL;
    }
}

echo 'Table Schemas Generated' . PHP_EOL;

