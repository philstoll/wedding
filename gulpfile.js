var gulp = require('gulp');
var gulpUglify = require('gulp-uglify');
var gulpConcat = require('gulp-concat');
var gulpMinifyCss = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');

gulp.task('css', function() {
    gulp.src([
        './node_modules/lightgallery/dist/css/lightgallery.css',
        './node_modules/justifiedGallery/dist/css/justifiedGallery.min.css',
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        './src/Assets/css/cover.css'
    ])
        .pipe(gulpConcat('main.min.css'))
        .pipe(gulpMinifyCss())
        .pipe(gulp.dest('./public/css'));
});

gulp.task('js', function() {
    gulp.src([
        './node_modules/jquery/dist/jquery.slim.min.js',
        './node_modules/justifiedGallery/dist/js/jquery.justifiedGallery.min.js',
        './node_modules/lightgallery/dist/js/lightgallery.min.js',
        './node_modules/popper.js/dis/popper.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js'
    ])
        .pipe(gulpConcat('main.min.js'))
        .pipe(gulpUglify())
        .pipe(gulp.dest('./public/js'));
});

gulp.task('image', function() {
    gulp.src('./src/Assets/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/images'));

    gulp.src('./src/Assets/images/thumbnails/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/images/thumbnails'));

    gulp.src('./node_modules/lightgallery/dist/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img'));
});

gulp.task('fonts', function() {
    gulp.src('./node_modules/lightgallery/dist/fonts/*')
        .pipe(gulp.dest('./public/fonts'));
});
