<?php

declare(strict_types=1);

namespace StollSoftware\Zend\Expressive\ZendView;

use Zend\Expressive\Template\TemplateRendererInterface;

class ConfigProvider
{
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    public function getDependencies() : array
    {
        return [
            'aliases' => [
                TemplateRendererInterface::class => StollSoftwareViewRenderer::class,
            ],
            'factories' => [
                StollSoftwareViewRenderer::class => StollSoftwareViewRenderFactory::class,
            ],
        ];
    }
}
