<?php

declare(strict_types=1);

namespace StollSoftware\Zend\Expressive\ZendView;

use Psr\Container\ContainerInterface;
use Zend\Expressive\ZendView\ZendViewRenderer;

class StollSoftwareViewRenderFactory
{
    public function __invoke(ContainerInterface $container) : StollSoftwareViewRenderer
    {
        $template = $container->get(ZendViewRenderer::class);

        $googleAnalyticsId = null;
        $config = $container->get('config');
        if (isset($config['analytics']) && isset($config['analytics']['google_analytics_id'])) {
            $googleAnalyticsId = $config['analytics']['google_analytics_id'];
        }

        return new StollSoftwareViewRenderer($template, $googleAnalyticsId);
    }
}
