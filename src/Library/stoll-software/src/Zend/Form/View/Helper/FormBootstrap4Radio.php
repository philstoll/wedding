<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace StollSoftware\Zend\Form\View\Helper;

use Zend\Form\Element\MultiCheckbox;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormRadio;

class FormBootstrap4Radio extends FormRadio
{
    public function render(ElementInterface $element)
    {
        if (! $element instanceof MultiCheckbox) {
            throw new \InvalidArgumentException(sprintf(
                '%s requires that the element is of type Zend\Form\Element\MultiCheckbox',
                __METHOD__
            ));
        }

        $name = static::getName($element);

        $radioValue = $element->getValue();
        $rendered = '';
        foreach ($element->getValueOptions() as $value => $option) {
            $rendered .= sprintf(
                '<div class="form-check form-check">%s%s</div>',
                $this->getInput($value, $name, $radioValue),
                $this->getLabel($option, $name, $value)
            );
        }

        return sprintf('<div class="col-form-label">%s</div>', $rendered);
    }

    private function getInput($value, $name, $elementValue)
    {
        $return = sprintf(
            '<input class="form-check-input" type="radio" name="%s" id="%s" value="%s" %s>',
            $name,
            $this->getIdFromNameAndValue($name, $value),
            $value,
            $this->getCheckedValue($value, $elementValue)
        );
        return $return;
    }

    private function getLabel($option, $name, $value)
    {
        $return = sprintf(
            '<label class="form-check-label" for="%s">%s</label>',
            $this->getIdFromNameAndValue($name, $value),
            $option
        );
        return $return;
    }

    private function getIdFromNameAndValue($name, $value)
    {
        $name = str_replace('[', '-', $name);
        $name = str_replace(']', '-', $name);

        return rtrim($name, '-') . '-' . $value;
    }

    private function getCheckedValue($optionValue, $elementValue)
    {
        if (null === $elementValue || $optionValue != $elementValue) {
            return '';
        }

        return 'checked';
    }
}
