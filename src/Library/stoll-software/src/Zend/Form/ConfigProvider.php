<?php

namespace StollSoftware\Zend\Form;

use Zend\ServiceManager\Factory\InvokableFactory;

class ConfigProvider
{
    /**
     * Return general-purpose zend-i18n configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'view_helpers' => $this->getViewHelperConfig(),
        ];
    }

    /**
     * Return zend-form helper configuration.
     *
     * Obsoletes View\HelperConfig.
     *
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'aliases' => [
                'formBootstrap4Radio' => View\Helper\FormBootstrap4Radio::class,
                'formElementErrors' => View\Helper\FormElementErrors::class,
                'FormElementErrors' => View\Helper\FormElementErrors::class,
                'formelementerrors' => View\Helper\FormElementErrors::class,
            ],
            'factories' => [
                View\Helper\FormBootstrap4Radio::class => InvokableFactory::class,
                View\Helper\FormElementErrors::class => InvokableFactory::class,
            ],
        ];
    }
}
