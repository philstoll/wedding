<?php
// src/App/Form/LoginForm.php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\Rsvp;
use Zend\Form\Element\Number;
use Zend\Form\Element\Radio;
use Zend\Form\Element\Textarea;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Between;

class RsvpFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('rsvp-fieldset');
        $this->addAllFieldsetElements();
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'attending',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],

            [
                'name' => 'events_attending',
                'required' => $this->isAttending(),
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],

            [
                'name' => 'number_attending',
                'required' => $this->isAttending(),
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => Between::class,
                        'options' => [
                            'min' => 1,
                            'max' => 10,
                        ],
                    ],
                ],
            ],

            [
                'name' => 'decline_names',
                'required' => ! $this->isAttending(),
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
        ];
    }

    private function addAllFieldsetElements()
    {
        $this->addAttendingRadio()
            ->addNumberAttendingText()
            ->addEventsAttendingRadio()
            ->addDeclinePersonsTextArea();

        return $this;
    }

    private function addNumberAttendingText()
    {
        $element = new Number('number_attending');
        $element->setLabel('Number Attending')
            ->setValue('2')
            ->setAttribute('class', 'form-control')
            ->setAttribute('min', '1')
            ->setAttribute('max', '10')
            ->setAttribute('onChange', 'adjustAttendees(this.value);')
            ->setLabelAttributes([
                'class' => $this->getDefaultLabelClassAttributes(),
            ]);
        $this->add($element);
        return $this;
    }

    private function addAttendingRadio()
    {
        $element = new Radio('attending');
        $element->setValueOptions(['1' => 'Yes', '0' => 'No'])
            ->setLabel('Will you be celebrating with us?')
            ->setLabelAttributes([
                'class' => $this->getDefaultLabelClassAttributes(),
            ]);
        $this->add($element);
        return $this;
    }

    private function addEventsAttendingRadio()
    {
        $element = new Radio('events_attending');
        $element->setValueOptions([
            Rsvp::EVENT_SET_CEREMONY_ONLY => 'Wedding Ceremony Only',
            Rsvp::EVENT_SET_RECEPTION_ONLY => 'Wedding Reception Only',
            Rsvp::EVENT_SET_BOTH => 'The Whole Shebang (Ceremony and Reception)'
        ])
            ->setLabel('Which events will you be attending?')
            ->setLabelAttributes([
                'class' => $this->getDefaultLabelClassAttributes(),
            ]);
        $this->add($element);
        return $this;
    }

    private function addDeclinePersonsTextArea()
    {
        $element = new Textarea('decline_names');
        $element->setLabel('Name(s)')
            ->setAttribute('class', 'form-control')
            ->setLabelAttributes([
                'class' => $this->getDefaultLabelClassAttributes(),
            ]);
        $this->add($element);
        return $this;
    }

    public function isAttending()
    {
        return $this->showAttendingSection();
    }

    private function getDefaultLabelClassAttributes()
    {
        return 'col-sm-3 col-form-label';
    }

    public function showNotAttendingSection()
    {
        $value = $this->get('attending')->getValue();
        return $value === '0';
    }

    public function showAttendingSection()
    {
        $value = $this->get('attending')->getValue();
        return $value === '1';
    }

    public function attendingIsSelected()
    {
        return true === $this->showNotAttendingSection() || true === $this->showAttendingSection();
    }
}
