<?php
// src/App/Form/LoginForm.php
declare(strict_types=1);

namespace App\Form;

use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Text;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class RsvpPersonFieldset extends Fieldset implements InputFilterProviderInterface
{
    private $required = true;

    public function __construct()
    {
        parent::__construct('rsvp-person-fieldset');
        $this->addAllFieldsetElements();
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'first_name',
                'required' => $this->required,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],

            [
                'name' => 'last_name',
                'required' => $this->required,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],

            [
                'name' => 'drinking_age',
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
        ];
    }

    private function addAllFieldsetElements()
    {
        $this->addFirstNameText()
            ->addLastNameText()
            ->addDrinkingAgeRadio();

        return $this;
    }

    private function addFirstNameText()
    {
        $element = new Text('first_name');
        $element->setLabel('First Name')
            ->setAttribute('placeholder', 'First Name')
            ->setAttribute('class', 'form-control');
        $this->add($element);
        return $this;
    }

    private function addLastNameText()
    {
        $element = new Text('last_name');
        $element->setLabel('Last Name')
            ->setAttribute('placeholder', 'Last Name')
            ->setAttribute('class', 'form-control');
        $this->add($element);
        return $this;
    }

    private function addDrinkingAgeRadio()
    {
        $element = new Checkbox('drinking_age');
        $element->setLabel('21 or over?')
            ->setValue(true);
        $this->add($element);
        return $this;
    }

    /**
     * @param bool $required
     * @return RsvpPersonFieldset
     */
    public function setRequired(bool $required): self
    {
        $this->required = $required;
        return $this;
    }
}
