<?php
// src/App/Form/LoginForm.php
declare(strict_types=1);

namespace App\Form;

use Zend\Form\Element\Collection;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;
use Zend\Form\Form;
use Zend\Hydrator\ArraySerializable;
use Zend\InputFilter\InputFilterProviderInterface;

class RsvpForm extends Form implements InputFilterProviderInterface
{
    private const DEFAULT_GUEST_COUNT = 2;
    public function __construct()
    {
        parent::__construct('rsvp-form');
    }

    public function init()
    {
        $this->setHydrator(new ArraySerializable());
        $this->addAllFormElements();
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'rsvp_csrf',
                'required' => true,
            ],
        ];
    }

    private function addAllFormElements() : self
    {
        $this->addCsrfElement()
            ->addRsvpFieldset()
            ->addRsvpPersonCollection()
            ->addSubmitButton();
        return $this;
    }

    private function addCsrfElement() : self
    {
        $element = new Csrf('rsvp_csrf');
        $element->setCsrfValidatorOptions(['timeout' => 1800]); //30 minutes
        $this->add($element);
        return $this;
    }

    private function addRsvpFieldset() : self
    {
        $fieldset = new RsvpFieldset();
        $this->add($fieldset);
        return $this;
    }

    private function addRsvpPersonCollection() : self
    {
        $collection = new Collection('rsvp-person');
        $collection->setTargetElement(new RsvpPersonFieldset())
            ->setCount(self::DEFAULT_GUEST_COUNT)
            ->setShouldCreateTemplate(true);
        $this->add($collection);
        return $this;
    }

    private function addSubmitButton() : self
    {
        $submit = new Submit('submit');
        $submit->setValue('Send RSVP')
            ->setAttribute('class', 'btn btn-primary');
        $this->add($submit);

        return $this;
    }

    /**
     * @param array|\ArrayAccess|\Traversable $data
     * @return $this|Form
     */
    public function setData($data)
    {
        parent::setData($data);
        $this->setRequiredFieldsOnCollectionElement();

        return $this;
    }

    public function getErrorMessage()
    {
        $errors = $this->getMessages();
        if (count($errors) === 0) {
            return '';
        }

        if (isset($errors['rsvp_csrf'])) {
            return '<strong>Form timeout</strong>. Please try again.';
        }

        return '<strong>Invalid submission</strong>. Please check the form for errors.';
    }

    private function setRequiredFieldsOnCollectionElement(): void
    {
        $this->get('rsvp-person')->getTargetElement()->setRequired($this->get('rsvp-fieldset')->isAttending());
    }
}
