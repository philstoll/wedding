<?php

namespace App\Mail;

use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;

class SmtpTransport extends Smtp
{
    public function __construct(SmtpOptions $options = null)
    {
        parent::__construct($options);
    }
}
