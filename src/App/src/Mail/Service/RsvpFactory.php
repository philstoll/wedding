<?php
namespace App\Mail\Service;

use App\Mail\SmtpTransport;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RsvpFactory
{
    /**
     * @param ContainerInterface $container
     * @return Rsvp
     */
    public function __invoke(ContainerInterface $container)
    {
        $transport = $container->get(SmtpTransport::class);
        $template = $container->get(TemplateRendererInterface::class);

        return new Rsvp($transport, $template);
    }
}
