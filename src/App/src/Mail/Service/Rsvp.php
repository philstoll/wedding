<?php

namespace App\Mail\Service;

use App\Model\Entity\Rsvp as RsvpEntity;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class Rsvp
{
    private $mailTransport;
    private $template;

    public function __construct(TransportInterface $mailTransport, TemplateRendererInterface $template)
    {
        $this->mailTransport = $mailTransport;
        $this->template = $template;
    }

    public function sendRsvpSavedMessage(RsvpEntity $rsvp)
    {
        $message = $this->buildSavedMessage(
            $this->template->render('mail::rsvp', ['rsvp' => $rsvp, 'layout' => false])
        )->setSubject($this->getSubject($rsvp->getAttending()));
        $this->mailTransport->send($message);
    }

    /**
     * @param string $htmlContent
     * @return Message
     */
    private function buildSavedMessage(string $htmlContent): Message
    {
        $message = new Message();

        //TODO: Build from config
        $message->addFrom('rsvp@kathrynandphil.com', 'Website Rsvp');
        $message->addTo('philstoll@gmail.com');
        $message->addTo('kathcald@gmail.com');

        $html = new MimePart($htmlContent);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $message->setBody($body);
        return $message;
    }

    /**
     * @param bool $attending
     * @return string
     */
    private function getSubject(bool $attending): string
    {
        if ($attending) {
            return 'Wedding RSVP - Attending';
        }

        return 'Wedding RSVP - Decline';
    }
}
