<?php
namespace App\Mail;

use Psr\Container\ContainerInterface;
use Zend\Mail\Transport\SmtpOptions;

class TransportFactory
{
    /**
     * @param ContainerInterface $container
     * @return SmtpTransport
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $smtpOptions = new SmtpOptions($config['mail']);

        return new SmtpTransport($smtpOptions);
    }
}
