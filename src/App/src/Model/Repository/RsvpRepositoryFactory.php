<?php

namespace Album\Model\Repository;

use App\Model\Repository\RsvpRepository;
use App\Model\Storage\RsvpStorageInterface;
use Interop\Container\ContainerInterface;

class AlbumRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return RsvpRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        return new RsvpRepository($container->get(RsvpStorageInterface::class));
    }
}
