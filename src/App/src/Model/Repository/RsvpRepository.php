<?php
namespace App\Model\Repository;

use App\Model\Entity\Rsvp;
use App\Model\Storage\RsvpStorageInterface;

class RsvpRepository
{
    private $rsvpStorage;

    public function __construct(RsvpStorageInterface $rsvpStorage)
    {
        $this->rsvpStorage = $rsvpStorage;
    }

    /**
     * @param Rsvp $rsvp
     * @return mixed
     */
    public function saveRsvp(Rsvp $rsvp)
    {
        return $this->rsvpStorage->insertRsvp($rsvp);
    }
}
