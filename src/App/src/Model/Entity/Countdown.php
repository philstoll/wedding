<?php

namespace App\Model\Entity;

use Zend\Stdlib\ArraySerializableInterface;

class Countdown implements ArraySerializableInterface
{
    const TIMEZONE = 'America/Chicago';

    private $hours;
    private $days;
    private $months;
    private $years;
    private $isFutureDate;

    public function __construct(\DateTime $date)
    {
        $timezone = new \DateTimeZone(self::TIMEZONE);
        $now = new \DateTime('now', $timezone);

        $countdown = $date->diff($now);

        $this->hours = $countdown->h;
        $this->days = $countdown->d;
        $this->months = $countdown->m;
        $this->years = $countdown->y;
        $this->isFutureDate = $now < $date;
    }

    /**
     * @return array
     */
    public function getArrayCopy(): array
    {
        return [
            'months' => $this->months,
            'days' => $this->days,
            'hours' => $this->hours,
            'years' => $this->years,
            'isFutureDate' => $this->isFutureDate,
        ];
    }

    public function exchangeArray(array $array)
    {
    }
}
