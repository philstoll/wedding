<?php

namespace App\Model\Entity;

use Zend\Stdlib\ArraySerializableInterface;

class Rsvp implements ArraySerializableInterface
{
    public const EVENT_SET_CEREMONY_ONLY = '1';
    public const EVENT_SET_RECEPTION_ONLY = '2';
    public const EVENT_SET_BOTH = '3';

    private $id;
    private $attending;
    private $eventSet;
    private $numberAttending;
    private $attendingPersons;
    private $notAttendingNames;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getAttending(): bool
    {
        return $this->attending;
    }

    /**
     * @param bool $attending
     * @return Rsvp
     */
    public function setAttending(bool $attending): self
    {
        $this->attending = $attending;
        return $this;
    }

    /**
     * @return string
     */
    public function getEventSet(): string
    {
        return $this->eventSet;
    }

    /**
     * @param string $eventSet
     * @return Rsvp
     */
    public function setEventSet(string $eventSet): self
    {
        $this->eventSet = $eventSet;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberAttending(): int
    {
        return $this->numberAttending;
    }

    /**
     * @param int $numberAttending
     * @return Rsvp
     */
    public function setNumberAttending(int $numberAttending): self
    {
        $this->numberAttending = $numberAttending;
        return $this;
    }

    /**
     * @return Person[]
     */
    public function getAttendingPersons(): array
    {
        return $this->attendingPersons;
    }

    /**
     * @param array $attendingPersons
     * @return Rsvp
     */
    public function setAttendingPersons(array $attendingPersons): self
    {
        $this->attendingPersons = [];
        foreach ($attendingPersons as $attendingPerson) {
            if (! $attendingPerson instanceof Person) {
                throw new \InvalidArgumentException(
                    sprintf('Expected instance of %s got %s', Person::class, get_class($attendingPerson))
                );
            }

            $this->addAttendingPerson($attendingPerson);
        }
        return $this;
    }

    public function addAttendingPerson(Person $person): self
    {
        $this->attendingPersons[] = $person;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotAttendingNames(): string
    {
        return $this->notAttendingNames;
    }

    /**
     * @param string $notAttendingNames
     * @return Rsvp
     */
    public function setNotAttendingNames(string $notAttendingNames): self
    {
        $this->notAttendingNames = $notAttendingNames;
        return $this;
    }

    public function getArrayCopy(): array
    {
        if ($this->getAttending()) {
            $results = [
                'attending' => 1,
                'event_set' => (int) $this->convertEventSetToId(),
                'number_attending' => $this->numberAttending,
            ];
        } else {
            $results = [
                'attending' => 0,
                'not_attending_names' => $this->notAttendingNames,
            ];
        }

        return $results;
    }

    public function exchangeArray(array $array)
    {
        $rsvpData = $array['rsvp-fieldset'];
        $this->setAttending((bool) $rsvpData['attending']);

        if ($this->attending) {
            $personsData = $array['rsvp-person'];
            $this->setAttendingFromExchangeArray($rsvpData, $personsData);
        } else {
            $this->setNotAttendingExchangeArray($rsvpData);
        }
    }

    private function setAttendingFromExchangeArray(array $rsvpArray, array $personArray): void
    {
        $this->setNumberAttending((int) $rsvpArray['number_attending'])
            ->setEventAttendingFromExchangeArray($rsvpArray['events_attending']);

        foreach ($personArray as $person) {
            $personEntity = new Person();
            $personEntity->setFirstName($person['first_name'])
                ->setLastName($person['last_name'])
                ->setDrinkingAge((bool) $person['drinking_age']);

            $this->addAttendingPerson($personEntity);
        }
    }

    private function setNotAttendingExchangeArray($rsvpData): void
    {
        $this->setNotAttendingNames($rsvpData['decline_names']);
    }

    private function setEventAttendingFromExchangeArray(string $value): void
    {
        switch ($value) {
            case self::EVENT_SET_CEREMONY_ONLY:
                $this->eventSet = 'Ceremony Only';
                break;
            case self::EVENT_SET_RECEPTION_ONLY:
                $this->eventSet = 'Reception Only';
                break;
            case self::EVENT_SET_BOTH:
                $this->eventSet = 'Ceremony and Reception';
                break;
            default:
                $this->eventSet = 'Ceremony and Reception';
        }
    }

    private function convertEventSetToId(): string
    {
        switch ($this->eventSet) {
            case 'Ceremony Only':
                return self::EVENT_SET_CEREMONY_ONLY;
            case 'Reception Only':
                return self::EVENT_SET_RECEPTION_ONLY;
            case 'Ceremony and Reception':
                return self::EVENT_SET_BOTH;
        }

        return null;
    }
}
