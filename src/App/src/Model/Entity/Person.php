<?php

namespace App\Model\Entity;

use Zend\Stdlib\ArraySerializableInterface;

class Person implements ArraySerializableInterface
{
    private $id;
    private $firstName;
    private $lastName;
    private $drinkingAge;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Person
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Person
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDrinkingAge(): bool
    {
        return $this->drinkingAge;
    }

    /**
     * @param bool $drinkingAge
     * @return Person
     */
    public function setDrinkingAge(bool $drinkingAge): self
    {
        $this->drinkingAge = $drinkingAge;
        return $this;
    }

    /**
     * @return array
     */
    public function getArrayCopy(): array
    {
        return [
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'drinking_age' => (int) $this->drinkingAge,
        ];
    }

    public function exchangeArray(array $array)
    {
    }
}
