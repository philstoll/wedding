<?php

namespace App\Model\Storage;

use App\Model\Entity\Rsvp;

interface RsvpStorageInterface
{
    public function insertRsvp(Rsvp $rsvp);
}
