<?php

namespace App\Model\Storage;

use App\Model\Entity\Person;

interface PersonStorageInterface
{
    public function insertPerson(Person $person);
}
