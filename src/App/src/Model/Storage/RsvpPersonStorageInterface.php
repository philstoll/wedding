<?php

namespace App\Model\Storage;

interface RsvpPersonStorageInterface
{
    public function insertRsvpPerson(int $rsvpId, int $personId);
}
