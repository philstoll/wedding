<?php
namespace App\Db;

use App\Model\Entity\Rsvp;
use App\Model\Storage\RsvpStorageInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\TableGateway\TableGateway;

class RsvpTableGateway extends TableGateway implements RsvpStorageInterface
{
    private $personTableGateway;
    private $rsvpPersonTableGateway;

    /**
     * RsvpTableGateway constructor.
     * @param AdapterInterface $adapter
     * @param ResultSetInterface $resultSet
     * @param PersonTableGateway $personTableGateway
     * @param RsvpPersonTableGateway $rsvpPersonTableGateway
     */
    public function __construct(
        AdapterInterface $adapter,
        ResultSetInterface $resultSet,
        PersonTableGateway $personTableGateway,
        RsvpPersonTableGateway $rsvpPersonTableGateway
    ) {

        $this->personTableGateway = $personTableGateway;
        $this->rsvpPersonTableGateway = $rsvpPersonTableGateway;
        parent::__construct('rsvp', $adapter, null, $resultSet);
    }

    /**
     * {@inheritDoc}
     */
    public function insertRsvp(Rsvp $rsvp)
    {
        if ($rsvp->getAttending()) {
            //Save Attending Record
            return $this->insertAttendingRsvp($rsvp->getArrayCopy(), $rsvp->getAttendingPersons());
        } else {
            //Save Not Attending Record
            return $this->insertNotAttendingRsvp($rsvp->getArrayCopy());
        }
    }

    private function insertAttendingRsvp(array $insertData, array $persons)
    {
        $insert = $this->getSql()->insert();
        $insert->values($insertData);
        $success = $this->insertWith($insert);
        $rsvpId = $this->lastInsertValue;

        //Insert Persons
        foreach ($persons as $person) {
            $insert = $this->getSql()->insert();
            $insert->values($person->getArrayCopy());
            $success = $this->personTableGateway->insertPerson($person);

            $personId = $this->personTableGateway->lastInsertValue;

            $this->rsvpPersonTableGateway->insertRsvpPerson($rsvpId, $personId);
        }

        return $rsvpId;
    }

    private function insertNotAttendingRsvp(array $insertData)
    {
        $insert = $this->getSql()->insert();
        $insert->values($insertData);
        $rsvpId = $this->insertWith($insert);

        return $rsvpId;
    }
}
