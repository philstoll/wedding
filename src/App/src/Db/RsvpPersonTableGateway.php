<?php
namespace App\Db;

use App\Model\Entity\Person;
use App\Model\Storage\PersonStorageInterface;
use App\Model\Storage\RsvpPersonStorageInterface;
use App\Model\Storage\RsvpStorageInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\TableGateway\TableGateway;

class RsvpPersonTableGateway extends TableGateway implements RsvpPersonStorageInterface
{
    /**
     * AlbumTableGateway constructor.
     *
     * @param AdapterInterface   $adapter
     * @param ResultSetInterface $resultSet
     */
    public function __construct(AdapterInterface $adapter, ResultSetInterface $resultSet)
    {
        parent::__construct('rsvp_person', $adapter, null, $resultSet);
    }

    /**
     * {@inheritDoc}
     */
    public function insertRsvpPerson(int $rsvpId, int $personId)
    {
        $insert = $this->getSql()->insert();
        $insert->values(['rsvp_id' => $rsvpId, 'person_id' => $personId]);
        $id = $this->insertWith($insert);

        return $id;
    }
}
