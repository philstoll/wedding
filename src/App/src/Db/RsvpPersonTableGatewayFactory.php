<?php
namespace App\Db;

use App\Model\Entity\Person;
use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Hydrator\ArraySerializable;

class RsvpPersonTableGatewayFactory
{
    /**
     * @param ContainerInterface $container
     * @return RsvpPersonTableGateway
     */
    public function __invoke(ContainerInterface $container)
    {
        $resultSetPrototype = new HydratingResultSet(
            new ArraySerializable(),
            new Person()
        );

        return new RsvpPersonTableGateway(
            $container->get(AdapterInterface::class),
            $resultSetPrototype
        );
    }
}
