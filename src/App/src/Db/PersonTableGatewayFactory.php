<?php
namespace App\Db;

use App\Model\Entity\Person;
use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Hydrator\ArraySerializable;

class PersonTableGatewayFactory
{
    /**
     * @param ContainerInterface $container
     * @return PersonTableGateway
     */
    public function __invoke(ContainerInterface $container)
    {
        $resultSetPrototype = new HydratingResultSet(
            new ArraySerializable(),
            new Person()
        );

        return new PersonTableGateway(
            $container->get(AdapterInterface::class),
            $resultSetPrototype
        );
    }
}
