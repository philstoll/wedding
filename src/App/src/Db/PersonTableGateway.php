<?php
namespace App\Db;

use App\Model\Entity\Person;
use App\Model\Storage\PersonStorageInterface;
use App\Model\Storage\RsvpStorageInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\TableGateway\TableGateway;

class PersonTableGateway extends TableGateway implements PersonStorageInterface
{
    /**
     * AlbumTableGateway constructor.
     *
     * @param AdapterInterface   $adapter
     * @param ResultSetInterface $resultSet
     */
    public function __construct(AdapterInterface $adapter, ResultSetInterface $resultSet)
    {
        parent::__construct('person', $adapter, null, $resultSet);
    }

    /**
     * {@inheritDoc}
     */
    public function insertPerson(Person $person)
    {
        $insert = $this->getSql()->insert();
        $insert->values($person->getArrayCopy());
        $id = $this->insertWith($insert);

        return $id;
    }
}
