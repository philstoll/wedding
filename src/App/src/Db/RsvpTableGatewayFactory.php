<?php
namespace App\Db;

use App\Model\Entity\Rsvp;
use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Hydrator\ArraySerializable;

class RsvpTableGatewayFactory
{
    /**
     * @param ContainerInterface $container
     * @return RsvpTableGateway
     */
    public function __invoke(ContainerInterface $container)
    {
        $resultSetPrototype = new HydratingResultSet(
            new ArraySerializable(),
            new Rsvp()
        );

        return new RsvpTableGateway(
            $container->get(AdapterInterface::class),
            $resultSetPrototype,
            $container->get(PersonTableGateway::class),
            $container->get(RsvpPersonTableGateway::class)
        );
    }
}
