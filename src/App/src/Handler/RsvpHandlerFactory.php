<?php

declare(strict_types=1);

namespace App\Handler;

use App\Db\RsvpTableGateway;
use App\Form\RsvpForm;
use App\Mail\Service\Rsvp;
use App\Model\Repository\RsvpRepository;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RsvpHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return MiddlewareInterface
     */
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        $template = $container->get(TemplateRendererInterface::class);
        $rsvpRepository = new RsvpRepository($container->get(RsvpTableGateway::class));
        $rsvpMailService = $container->get(Rsvp::class);

        /* @var $formManager \Zend\Form\FormElementManager\FormElementManagerV3Polyfill */
        $formManager =$container->get('FormElementManager');
        $formManager->setShared(RsvpForm::class, true);
        $form = $formManager->get(RsvpForm::class);

        return new RsvpHandler($template, $rsvpRepository, $form, $rsvpMailService);
    }
}
