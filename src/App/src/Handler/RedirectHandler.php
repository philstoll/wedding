<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;

class RedirectHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        /* @var $result \Zend\Expressive\Router\RouteResult */
        $redirectUrl = $this->getRedirectUrl($request->getAttribute('Zend\Expressive\Router\RouteResult'));

        return new RedirectResponse($redirectUrl);
    }

    /**
     * @param Router\RouteResult $routeResult
     * @return string
     */
    private function getRedirectUrl(Router\RouteResult $routeResult): string
    {
        switch ($routeResult->getMatchedRouteName()) {
            case 'bbb':
                return 'https://www.bedbathandbeyond.com/store/giftregistry/view_registry_guest.jsp?'
                    . 'pwsToken=&eventType=Wedding&inventoryCallEnabled=true&registryId=545815924';
            case 'amazon':
                return 'https://smile.amazon.com/wedding/kathryn-caldwell-phil-stoll-columbia-october-2018'
                    . '/registry/14F7589FZ109T';
            case 'honeymoon':
            default:
                return 'https://playaresorts.honeymoonwishes.com/Honeymoon-Registry-377817-'
                    . 'THE-Royal-Playa-del-Carmen-Kathryn-Caldwell-Phil-Stoll.html';
        }
    }
}
