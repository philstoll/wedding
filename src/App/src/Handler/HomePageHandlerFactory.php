<?php

declare(strict_types=1);

namespace App\Handler;

use App\Model\Entity\Countdown;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class HomePageHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $config = $container->get('config');
        $timezone = new \DateTimeZone($config['system']['timezone']);
        $weddingDate = new \DateTime($config['system']['wedding_date'], $timezone);
        $countdown = new Countdown($weddingDate);
        $template = $container->get(TemplateRendererInterface::class);

        return new HomePageHandler($countdown, $template);
    }
}
