<?php

declare(strict_types=1);

namespace App\Handler;

use App\Model\Entity\Rsvp;
use App\Form\RsvpForm;
use App\Mail\Service\Rsvp as RsvpMailService;
use App\Model\Repository\RsvpRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class RsvpHandler implements MiddlewareInterface
{
    private $template;
    private $rsvpRepository;
    private $form;
    private $mailService;

    public function __construct(
        TemplateRendererInterface $template,
        RsvpRepository $rsvpRepository,
        RsvpForm $form,
        RsvpMailService $mailService
    ) {

        $this->template = $template;
        $this->rsvpRepository = $rsvpRepository;
        $this->form = $form;
        $this->mailService = $mailService;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $this->form->setData($request->getParsedBody());

        if ($this->form->isValid()) {
            try {
                $rsvpEntity = new Rsvp();
                $rsvpEntity->exchangeArray($this->form->getData());

                if ($this->rsvpRepository->saveRsvp($rsvpEntity)) {
                    $this->mailService->sendRsvpSavedMessage($rsvpEntity);

                    //TODO:: Add Flash messaging
                    return new RedirectResponse('/photos?attending=' . (int) $rsvpEntity->getAttending());
                }
            } catch (\Exception $e) {
                $this->form->setOption(
                    'error-message',
                    'Reservation submission failed. Please try again. <br>'
                    . 'If you continue to get an error please email us at '
                    . '<a href=mailto:kathcald@gmail.com">kathcald@gmail.com</a>'
                );

                $response = $handler->handle($request);
                return $response;
            }
        }

        $this->form->setOption(
            'error-message',
            $this->form->getErrorMessage()
        );

        $response = $handler->handle($request);
        return $response;
    }
}
