<?php

declare(strict_types=1);

namespace App\Handler;

use App\Form\RsvpForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class RsvpPage implements RequestHandlerInterface
{
    private $template;
    private $form;

    public function __construct(Template\TemplateRendererInterface $template, RsvpForm $form)
    {
        $this->template = $template;
        $this->form = $form;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $this->template->addDefaultParam($this->template::TEMPLATE_ALL, 'primary_navigation', 'rsvp');
        return new HtmlResponse($this->template->render('app::rsvp', ['form' => $this->form]));
    }
}
