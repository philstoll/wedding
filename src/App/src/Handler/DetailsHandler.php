<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

class DetailsHandler implements RequestHandlerInterface
{
    private $template;

    public function __construct(Template\TemplateRendererInterface $template)
    {
        $this->template      = $template;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $this->template->addDefaultParam($this->template::TEMPLATE_ALL, 'primary_navigation', 'details');
        return new HtmlResponse(
            $this->template->render('app::details', ['wedding_party' => $this->getWeddingParty()])
        );
    }

    private function getWeddingParty()
    {
        return [
            'bridesmaids' => [
                [
                    'name' => 'Lauren Bianco',
                    'title' => 'Friend of the bride',
                ],
                [
                    'name' => 'Sarah Johnson',
                    'title' => 'Friend of the bride',
                ],
                [
                    'name' => 'Carla Basler',
                    'title' => 'Sister of the groom',
                ],
            ],
            'groomsmen' => [
                [
                    'name' => 'Keith Basler',
                    'title' => 'Friend of the groom',
                ],
                [
                    'name' => 'Brad Thornsberry',
                    'title' => 'Friend of the groom',
                ],
                [
                    'name' => 'Kyle Basler',
                    'title' => 'Brother-in-law of the groom',
                ],
            ],
        ];
    }
}
