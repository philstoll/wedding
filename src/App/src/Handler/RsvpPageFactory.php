<?php

declare(strict_types=1);

namespace App\Handler;

use App\Form\RsvpForm;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RsvpPageFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $template = $container->get(TemplateRendererInterface::class);
        $form = $container->get('FormElementManager')
            ->get(RsvpForm::class);

        return new RsvpPage($template, $form);
    }
}
