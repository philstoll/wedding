<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;
use App\Model\Entity\Countdown;

class HomePageHandler implements RequestHandlerInterface
{

    private $countdown;
    private $template;

    public function __construct(Countdown $countdown, Template\TemplateRendererInterface $template)
    {
        $this->countdown = $countdown;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $data = $this->countdown->getArrayCopy();
        $data['layout'] = 'layout::home-page';

        $this->template->addDefaultParam($this->template::TEMPLATE_ALL, 'primary_navigation', 'home');
        return new HtmlResponse($this->template->render('app::home-page', $data));
    }
}
