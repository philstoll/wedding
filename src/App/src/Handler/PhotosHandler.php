<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;

class PhotosHandler implements RequestHandlerInterface
{
    private $containerName;
    private $router;
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        Template\TemplateRendererInterface $template = null,
        string $containerName
    ) {
        $this->router        = $router;
        $this->template      = $template;
        $this->containerName = $containerName;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $queryParams = $request->getQueryParams();
        $data = [];
        if (isset($queryParams['attending'])) {
            $data['attending'] = (bool) $queryParams['attending'];
        }
        $this->template->addDefaultParam($this->template::TEMPLATE_ALL, 'primary_navigation', 'photos');
        return new HtmlResponse($this->template->render('app::photos', $data));
    }
}
