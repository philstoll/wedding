<?php

declare(strict_types=1);

namespace App;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'factories'  => [
                Handler\ComingSoonHandler::class => Handler\ComingSoonHandlerFactory::class,
                Handler\DetailsHandler::class => Handler\DetailsHandlerFactory::class,
                Handler\HomePageHandler::class => Handler\HomePageHandlerFactory::class,
                Handler\PhotosHandler::class => Handler\PhotosHandlerFactory::class,
                Handler\RegistryHandler::class => Handler\RegistryHandlerFactory::class,
                Handler\RsvpPage::class => Handler\RsvpPageFactory::class,
                Handler\RsvpHandler::class => Handler\RsvpHandlerFactory::class,

                Db\RsvpTableGateway::class => Db\RsvpTableGatewayFactory::class,
                Db\PersonTableGateway::class => Db\PersonTableGatewayFactory::class,
                Db\RsvpPersonTableGateway::class => Db\RsvpPersonTableGatewayFactory::class,

                Mail\SmtpTransport::class => Mail\TransportFactory::class,
                Mail\Service\Rsvp::class => Mail\Service\RsvpFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'app'    => [__DIR__ . '/../templates/app'],
                'error'  => [__DIR__ . '/../templates/error'],
                'layout' => [__DIR__ . '/../templates/layout'],
                'mail' => [__DIR__ . '/../templates/mail'],
            ],
        ];
    }
}
